import os
import itertools as it
import time 	
import re
import tasks

prism_path='~/Codes_Softwares/prism-4.4-linux64/bin/prism' 			#locate prism


#def MDP_script(job):	



def Criticality_script(job,threshold,no_crit,no_jobs):
	no_hcjobs=0
	for i in range(0,len(job)):
		if job[i].taskno<=no_crit:				#< or ==
			no_hcjobs=no_hcjobs+1
	header='dtmc\n'
	states='x:[0..{}] init 0;\n'.format(2+(no_hcjobs-1)*2) 
	file1=open("RESULTS/Model_MDP","a") 
	file1.write(header)
	file1.write("module M\n")
	file1.write(states)
	maxprob=tasks.Job(0,0,0,0,[0.0]*int(no_jobs))
	mxprob=0
	statecount=0
	for i in range(0,len(job)):
		if job[i].taskno<=no_crit:				#< or ==
			prop_dm='1-P=? [F={} x={}]\n'.format((job[i].deadline-job[i].arrival)*threshold,0)
	#		else:
	#			prop_dm='1-P=? [F={} x={}]\n'.format((job.deadline-job.arrival),0)
		
			file = open("Prism_Files/Property_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"w") 
			file.write(prop_dm)
			file.close() 
			
			command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_Crit_{}_{} > Prism_Files/Result_Job_Crit_{}_{} ".format(prism_path,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno)				
			os.system(command)
			while not os.path.exists("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				time.sleep(1)
			if os.path.isfile("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				file = open("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"r")
				text=file.read()
				file.close()
				res_pos=text.find('Result') 	
				if res_pos==-1:
					prob=-1
				else:
					line=text[res_pos:res_pos+55]
					val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
					prob=float(val[0])	
			else:
			   	raise ValueError("%s isn't a file!" % file_path)
					

			if prob>mxprob and (job[i].taskno>0 or job[i].jobno>0) :
				mxprob=prob 
				maxprob=job[i]
#			comment="\\\\{},{}\n \n".format(job[i].taskno,job[i].jobno)
			print('prob: ',prob)


			d=Obtain_D(i,job,prob,threshold)
			prob=prob+d
			print('prob after: ',prob)


			if i==0 :
				statement00="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,prob,statecount+1,1-prob,statecount+2) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,statecount+1,1,statecount+2) 
				statecount=statecount+1

			else:
				statement01="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,prob,statecount+2,1-prob,statecount+3) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,statecount+1,1,statecount+2) 
	
				statement02="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount+1,prob,statecount+2,1-prob,statecount+3) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,statecount+1,1,statecount+2) 


			
#			statement1="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,prob,statecount+1,1-prob,statecount+2) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,statecount+1,1,statecount+2) 

 #if prob!= -1 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,statecount+1,1,statecount+2)
#			statement2="[] x={} -> 1:(x'={}) ;\n".format(statecount+1,statecount+3)
#			statement3="[] x={} -> 1:(x'={}) ;\n".format(statecount+2,statecount+3)

				statecount=statecount+2
	
			print('statecount', statecount)
	
			if i==0 :
				file1.write(statement00)
			else:
				file1.write(statement01)
				file1.write(statement02)

	file1.write('endmodule')
	file1.close
	return maxprob,mxprob



def Obtain_D(n,job,p_hi,threshold):
	no_non_zero=0
	v=0
	for i in range(0,len(job)):
		if job[i].dep[n]!=0:
			no_non_zero=no_non_zero+1

			prop_dm='1-P=? [F={} x={}]\n'.format((job[i].deadline-job[i].arrival)*threshold,0)
	#		else:
	#			prop_dm='1-P=? [F={} x={}]\n'.format((job.deadline-job.arrival),0)
		
			file = open("Prism_Files/Property_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"w") 
			file.write(prop_dm)
			file.close() 
			
			command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_Crit_{}_{} > Prism_Files/Result_Job_Crit_{}_{} ".format(prism_path,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno)				
			os.system(command)
			while not os.path.exists("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				time.sleep(1)
			if os.path.isfile("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				file = open("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"r")
				text=file.read()
				file.close()
				res_pos=text.find('Result') 	
				if res_pos==-1:
					prob=-1
				else:
					line=text[res_pos:res_pos+55]
					val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
					prob=float(val[0])	
			else:
			   	raise ValueError("%s isn't a file!" % file_path)
					
			v=v+job[i].dep[n]*prob
	print('no_non_zero: ',no_non_zero)
	v=(1-p_hi)*(v/no_non_zero) if no_non_zero>0 else 0
	print('V: ',v)
	return v




def Criticality_analysis(job,no_crit):

	no_hcjobs=0
	for i in range(0,len(job)):
		if job[i].taskno<=no_crit:		#< or ==
			no_hcjobs=no_hcjobs+1
	print('no_hcjobs: ',no_hcjobs)

	n=2**(no_hcjobs)
#	n=9
	k=3


	l=no_hcjobs+1
	
	statebin=[]	
	ind1=0
	for i in range(0,n):
		posspath=bin(i)
		s=0
		for j in range(2,len(posspath)):
			s=s+int(posspath[j])
#			print('s here= ' , s)
		if s != k:				 # <= 9-k:
			statebin.append(bin(i))   #bin(2**(i)))
			statebin[ind1]=str(statebin[ind1][2:]).zfill(l-1)
			ind1=ind1+1

	print(statebin)
	percent=1
	Total_prob=0
	for i in range(0,ind1): 	#n-1):
		prop=' Pmax=? [ '
		path=statebin[i]
		ind=0
		for j in range(0,no_hcjobs):			
#			print('j: ',j)
			if path[j]=='1':
				st=ind+2
			else:
				st=ind+1
			ind=ind+2

			next='X'
			for k in range(0,j):
				next=next+' X'
	
			statement='({} x={}) & '.format(next,st) if j<no_hcjobs-1 else '({} x={})'.format(next,st)
			prop=prop+statement
		
		prop=prop+']'
		print(prop)
		
		file = open("Prism_Files/Property_MDP","w") 
		file.write(prop)
		file.close() 


		command="{} -cuddmaxmem 8g RESULTS/Model_MDP Prism_Files/Property_MDP  > RESULTS/Result_MDP ".format(prism_path)
		os.system(command)
		while not os.path.exists("RESULTS/Result_MDP"):
			time.sleep(1)
		if os.path.isfile("RESULTS/Result_MDP"):
			file = open("RESULTS/Result_MDP","r")
			text=file.read()
			file.close()
			res_pos=text.find('Result') 	
			if res_pos==-1:
				prob=-1
			else:
				line=text[res_pos:res_pos+55]
				val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
				prob=float(val[0])	
		else:
		   	raise ValueError("%s isn't a file!" % file_path)
				
		print('value: ', prob)
		print('Progress: ', percent, ' of ', len(statebin)) # round((percent+(i/ind))*100,2),'% \n')
		percent=percent+1
		Total_prob=Total_prob+prob
		print('Total: ',Total_prob)

	
	print('Summation of all the high criticality paths: ',Total_prob)



def prob_back(job):
	print('-------------------Probablities of intercation/delay--------------')
	prob1=0
#	flag=0
	for i in range(0,len(job)-1):
		interval=job[i].deadline-job[i+1].arrival if job[i].deadline-job[i+1].arrival>0 else job[i].deadline-job[i].arrival 
		prop='1- P=? [F[{},{}] x=0] '.format(job[i+1].arrival,job[i+1].arrival+interval)
		file = open("Prism_Files/Property_Drop_Job_{}_{}".format(job[i].taskno,job[i].jobno),"w") 
		file.write(prop)
		file.close() 

		command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Drop_Job_{}_{}  > RESULTS/Result_Drop_Job_{}_{} ".format(prism_path,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno)
		os.system(command)
	
		while not os.path.exists("RESULTS/Result_Drop_Job_{}_{}".format(job[i].taskno,job[i].jobno)):
			time.sleep(1)
		if os.path.isfile("RESULTS/Result_Drop_Job_{}_{}".format(job[i].taskno,job[i].jobno)):
			file = open("RESULTS/Result_Drop_Job_{}_{}".format(job[i].taskno,job[i].jobno),"r")
			text=file.read()
			file.close()
			res_pos=text.find('Result') 	
			if res_pos==-1:
				prob=-1
			else:
				line=text[res_pos:res_pos+55]
				val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
				prob=float(val[0])	
		else:
		   	raise ValueError("%s isn't a file!" % file_path)
				
		job[i].printparam()
#		flag=1 if job[i].taskno>no
		print('Interval: ', interval, ', Prob interacation: ',prob)
		if prob>prob1 and i>1:
			prob1=prob
			print('HIGHEST YET')	











