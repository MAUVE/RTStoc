import tasks
import numpy as np
import math 

class Arc:
	def __init__(self,from_job,to_job):
		self.from_job=from_job
		self.to_job=to_job

class Tree:
	def __init__(self,no,from_node,the_job,rt_node):
		self.no=no
		self.from_node=from_node
		self.the_job=the_job
#		self.to_job=to_job
		self.to_node=[]
		self.rt_node=rt_node
	def append_tonode(self,no):
		self.to_node.append(no)
	def print_tonode(self):
		print(self.to_node)

	def checklen_tonode(self):
		return len(self.to_node)


def no_repitions(the_job,tree1,count_r):
	while count_r>=0:
		if tree1[count_r].the_job.taskno==the_job.taskno and tree1[count_r].the_job.jobno==the_job.jobno and tree1[count_r].the_job.crit==the_job.crit:
#			print('rep at: ID: ',tree1[count_r].no, 'Task',tree1[count_r].the_job.taskno+1,'Job: ',tree1[count_r].the_job.jobno+1,'Crit: ',tree1[count_r].the_job.crit)
			return 1
		else:
			count_r=tree1[count_r].from_node
#			print('count_r=tree1[count_r].from_node',count_r)
#	print('no rep')
	return 0
	

def check_later_jobs(the_job,tree1,count_r): 
#	print('count_r',count_r)
#	print('HERE in check_later_jobs(')
#	print('the_job.jobno:',the_job.jobno )
	if the_job.jobno>0:
#		print('In while')
		while count_r>=0:
			
			if tree1[count_r].the_job.taskno==the_job.taskno and tree1[count_r].the_job.jobno==the_job.jobno-1 :# and tree1[count_r].the_job.crit==the_job.crit:
#				print('YES at:',tree1[count_r].no)
				return 1
			else:
				count_r=tree1[count_r].from_node
#		print('RETURNED 0 !')
		return 0
	else:
		return 1



def isok(node_i,node_j):
	answer=1				# 1 is YES, 0 is NO

	if node_i.taskno==node_j.taskno and node_i.jobno==node_j.jobno and  node_i.crit==node_j.crit:	
		answer=0
	if node_i.taskno==node_j.taskno and node_i.jobno==node_j.jobno and  node_i.crit>node_j.crit:	
		answer=0
	
	if  node_j.deadline<=node_i.arrival:							
		answer=0	

	if node_i.taskno==node_j.taskno and node_j.jobno>node_i.jobno+1:
		answer=0

	if  node_j.crit>0 and sametaskjob(node_i,node_j)==0:
		answer=0	
	
	if  node_j.crit>node_i.crit+1:#  and sametaskjob(node_i,node_j)==0 :
		answer=0
	
	

	
	return answer

def sametaskjob(node_i,node_j):
	if node_i.taskno==node_j.taskno and node_i.jobno==node_j.jobno:
		return 1
	else:
		return 0


def script_graph(arc,nodes):
	
	l1="digraph nodes { \n"
	l3="\n}"
	l2=" "
	
	for i in range(0,len(arc)):
		print(i,': From: ',arc[i].from_job.taskno+1, ' ', arc[i].from_job.jobno+1, 'To: ',arc[i].to_job.taskno+1,' ',arc[i].to_job.jobno+1,'crit_from ', arc[i].from_job.crit,'crit_to', arc[i].to_job.crit)	
		
#	for i in range(0,len(nodes)):
#		for j in range(0,len(arc)):
#			if arc[j].from_job.taskno==nodes[i].taskno and arc[j].from_job.jobno==nodes[i].jobno and arc[j].from_job.crit==nodes[i].crit:
#				l2=l2+ " \"  node_{}_{}_crit_{} \"  ".format(nodes[i].taskno+1,nodes[i].jobno+1,nodes[i].crit)
#
#				if j<len(arc)-1:
#					l2=l2+"->"
#	l2=l2+"\n"
#	print(l1+l2+l3)








def Paths( tree_node,tree,hi_jobs,mi_jobs,lo_jobs,max_mijobs,max_lojobs,hp,job,task):
	path=[]
	j=-1
	done=0
	ut=0
	while done==0:
#			print('apending paths ',j)
		
			path.append(Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0))   
			j=j+1
			path[j].no=j
			path[j].from_node=tree_node.no
			path[j].to_node=tree_node.from_node
			#path[0].to_node.append(-1)
	#		path[j].append_tonode(-100)
			path[j].the_job.taskno=tree_node.the_job.taskno
			path[j].the_job.jobno=tree_node.the_job.jobno
			path[j].the_job.dist=tree_node.the_job.dist
			path[j].the_job.arrival=tree_node.the_job.arrival
			path[j].the_job.deadline=tree_node.the_job.deadline
			path[j].the_job.priority=tree_node.the_job.priority
			path[j].the_job.crit=tree_node.the_job.crit
			path[j].rt_node=tree_node.rt_node
			print('task: ',path[j].the_job.taskno+1, ' job: ',path[j].the_job.jobno+1,'crit: ',path[j].the_job.crit, 'From: ',path[j].from_node)

			
			tree_node=tree[tree_node.from_node]  
					

			if tree_node.from_node<0 : #and :
				done=1
				path.append(Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0))   
				j=j+1
				path[j].no=j
				path[j].from_node=tree_node.no
				path[j].to_node=tree_node.from_node
				#path[0].to_node.append(-1)
		#		path[j].append_tonode(-100)
				path[j].the_job.taskno=tree_node.the_job.taskno
				path[j].the_job.jobno=tree_node.the_job.jobno
				path[j].the_job.dist=tree_node.the_job.dist
				path[j].the_job.arrival=tree_node.the_job.arrival
				path[j].the_job.deadline=tree_node.the_job.deadline
				path[j].the_job.priority=tree_node.the_job.priority
				path[j].the_job.crit=tree_node.the_job.crit
				path[j].rt_node=tree_node.rt_node
				

				print('task: ',path[j].the_job.taskno+1, ' job: ',path[j].the_job.jobno+1,'crit: ',path[j].the_job.crit, 'From: ',path[j].from_node)



	nodes_sched=[]
	if len(path)==len(job) : #or check_for_hi(path,task)==1:
		psys=0
		sen='\n--------------------------------------------------------------------------------------------------\n\n'	
		for j in range(0,len(path)) :
			prdm=dm(path[j])[0]
			if path[j].the_job.taskno==0 : #path[j].the_job.crit>-1 and 
				prhi=threshold(path[j])
			else:
				prhi='NA'

			if path[j].the_job.taskno==0 : # or ....
				psys=psys+prhi
		
			
			sen=sen+('task: {} job: {} crit: {} RT: {} prDM:{} prHI:{} ------- From: {}\n').format(path[j].the_job.taskno+1,path[j].the_job.jobno+1,path[j].the_job.crit,path[j].rt_node,prdm,prhi,path[j].from_node)
			nodes_sched.append(path[j])
#		sen=sen+('mi_jobs: {} lo_jobs: {} \n--------- Utilization: {}').format(no_mijobs,no_lojobs,ut)
#		sen=sen+'\n\n------------------------------------\n\nGraph/Valid and non dangerous paths in LO Criticality\n\n'
		sen=sen+('\nProbability System entering HI : {} \n\n').format(psys/6)
		if psys/6 < 0.007:
			file = open("Graph/Valid and non dangerous paths",'a') 
			file.write(sen)
			file.close() 


#	print('MAX_mi_jobs: ',max_mijobs,'MAX_lo_jobs: ',max_lojobs, 'nodes_sched: ',nodes_sched)
	return nodes_sched
	


def  check_for_hi(path,task):
	no_hi=0
	check_hi=0
	for i in range(0,len(task)):
		if task[i].crit==2:
			no_hi=no_hi+1
	for i in range(0,len(path)):
		if path[i].the_job.crit==2:
			check_hi=check_hi+1
	if check_hi==no_hi:
		return 1
	else:
		return 0





def wcet_values_MI(node):
	if node.taskno==0: 
		return 0.5
	elif node.taskno==1: 
		return 4
	elif node.taskno==3: 
		return 4
	elif node.taskno==5: 
		return 1
	else :
		return 0

def wcet_values_HI (node):
	if node.taskno==0: 
		return 1
	else :
		return 0


def Construct_Model( initial_node,arc,nodes,hi_jobs,mi_jobs,lo_jobs,hp ,job,task):

	tree1=[]
	

	count=0

	tree1.append(Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0))   # id, from_id, the_job, to_id

	tree1[0].no=0
	tree1[0].from_node=-100
	#tree1[0].to_node=[]
	#tree1[0].append_tonode(-1)
	tree1[0].the_job.taskno=initial_node.taskno
	tree1[0].the_job.jobno=initial_node.jobno
	tree1[0].the_job.dist=initial_node.dist
	tree1[0].the_job.arrival=initial_node.arrival
	tree1[0].the_job.deadline=initial_node.deadline
	tree1[0].the_job.priority=initial_node.priority
	tree1[0].the_job.crit=initial_node.crit
	tree1[0].rt_node=initial_node.dist #+wcet_values_MI(initial_node)+wcet_values_HI(initial_node)
	print('RT_node: ',tree1[0].rt_node)

	
	#print('tree1[0].the_job.arrival=initial_node[0].arrival',tree1[0].the_job.arrival, ' t ',tree1[0].the_job.taskno, ' j' ,tree1[0].the_job.jobno)
	
	count=0
	count_r=0
	done=0
	run=0
	
	while done!=1:
		print('\n---->: ID',tree1[count_r].no ,' : ',' task: ',tree1[count_r].the_job.taskno+1, ' job: ',tree1[count_r].the_job.jobno+1,'crit: ',tree1[count_r].the_job.crit,'RT: ',tree1[count_r].rt_node, '---->')			
	
		for i in range(0,len(arc)):
		
			if arc[i].from_job.taskno==tree1[count_r].the_job.taskno and arc[i].from_job.jobno==tree1[count_r].the_job.jobno  and no_repitions(arc[i].to_job,tree1,count_r)!=1  and check_later_jobs(arc[i].to_job,tree1,count_r)==1 :# and arc[i].from_job.crit==tree1	[count_r].the_job.crit # and arc[i].to_job.arrival<tree1[count_r].the_job.deadline: 

				if dm(tree1[count_r])[1]==1:
#tree1[count_r].rt_node>tree1[count_r].the_job.deadline-tree1[count_r].the_job.arrival:
					print('------------------------------------------------------------------------------DEADLINE MISS  !!!')
					break

				tree1.append(Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0)) 
				count=count+1

				tree1[count].no=count
				tree1[count].from_node=tree1[count_r].no
				print('[',tree1[count].from_node,']')
#					tree1[count_r].to_node=[]
				tree1[count_r].to_node.append(count)
	
				print(tree1[count_r].to_node)
				
				tree1[count].the_job.taskno=arc[i].to_job.taskno
				tree1[count].the_job.jobno=arc[i].to_job.jobno
				tree1[count].the_job.dist=arc[i].to_job.dist
				tree1[count].the_job.arrival=arc[i].to_job.arrival
				tree1[count].the_job.deadline=arc[i].to_job.deadline
				tree1[count].the_job.priority=arc[i].to_job.priority
				tree1[count].the_job.crit=arc[i].to_job.crit

					
				tree1[count].rt_node=Calculate_RT(tree1[count],tree1[count_r],arc[i])

	
				if tree1[count].the_job.taskno==0 and tree1[count].the_job.jobno==1:
					print('Dependendce')
					tree1[count].rt_node=Calculate_RT_Dependent(tree1[count],tree1,0,0)






#				if tree1[count].rt_node>tree1[count].the_job.deadline-tree1[count].the_job.arrival:
#					tree1[count_r].from_node=-100
	

#				print('RT: ',tree1[count].rt_node) #, ' MAX :',max((tree1[count_r].rt_node+tree1[count_r].the_job.arrival-tree1[count].the_job.arrival),0))

				print('ID: ',tree1[count].no,':','task: ',tree1[count].the_job.taskno+1, 'job: ',tree1[count].the_job.jobno+1,'crit: ',tree1[count].the_job.crit, 'RT: ',tree1[count].rt_node )

		if count==count_r:
			print(count_r)
			print(count)
			done=done+1
	#	if run>1:
		count_r=count_r+1
	#	run=run+1
		
	
	print('len: ', len(tree1))		

		
#	for i in range(0,len(tree1)):
#		print('Summarize: ID:',tree1[i].no,':','task: ',tree1[i].the_job.taskno+1, ' job: ',tree1[i].the_job.jobno+1,'crit: ',tree1[i].the_job.crit,'RT: ',tree1[i].rt_node)






	print('------------------------------------------------------------------PATHS------------------------------------------------------------------')
		
	path=[]
	path.append(Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0))   
	path[0].no=0
	path[0].from_node=-100
	path[0].append_tonode(-1)
	path[0].the_job.taskno=nodes[0].taskno
	path[0].the_job.jobno=nodes[0].jobno
	path[0].the_job.dist=nodes[0].dist
	path[0].the_job.arrival=nodes[0].arrival
	path[0].the_job.deadline=nodes[0].deadline
	path[0].the_job.priority=nodes[0].priority
	path[0].the_job.crit=nodes[0].crit
	path[0].rt_node=nodes[0].dist #+wcet_values_MI(nodes[0])+wcet_values_HI(nodes[0])


	done=0
	j=0
	no_paths=0
	max_mijobs=0
	max_lojobs=0


	nodes_sched=[]
	for i in range(0,len(tree1)):
		if tree1[i].checklen_tonode()==0 and dm(tree1[i])[1]==0:
			if tree1[i].the_job.crit>=0:
				print('AT PATHS FUNCTION')
				nodes_sched1=Paths(tree1[i],tree1,hi_jobs,mi_jobs,lo_jobs,max_mijobs,max_lojobs,hp,job,task)
				if len(nodes_sched1)>=len(nodes_sched):
					nodes_sched=nodes_sched1
#				print('nodes_sched',nodes_sched[0])
			no_paths=no_paths+1
			print('------------------------')
	print('no of paths: ',no_paths)

	file = open("Graph/Valid and non dangerous paths",'a') 
	if len(nodes_sched)==0:
		file.write('\n\n NO SCHEDULE POSSIBLE, PLEASE IMPROVE PARAMETERS')
	file.write('\n\n\n------------------------------------------------------------------------------------------\n------------------------------------------------------------------------------------------\n\n')

	file.close() 
	path_forcrit_MAX=[]
	if len(nodes_sched)==0:
		return 
#	print('Length of shed ndes',len(nodes_sched))
	for k in range(0,len(nodes_sched)):	
		print('ID', nodes_sched[k].from_node,'task:',nodes_sched[k].the_job.taskno+1, 'job: ',nodes_sched[k].the_job.jobno+1)

	
#	for k in range(0,len(nodes)):
#		if nodes[k].crit>0:
#			Paths_Selective(tree1,nodes[k],nodes_sched,hi_jobs,mi_jobs,lo_jobs)
#			Return_optimal_path( tree1,nodes[k],nodes_sched,hi_jobs,mi_jobs,lo_jobs)

	file = open("Graph/Valid and non dangerous paths",'a') 
	file.write('LENGTH: ',len(tree1))
	file.close() 


def Calculate_RT(tree_add,tree_parent,arc):
	x=tail(max(tree_add.the_job.arrival-tree_parent.the_job.arrival,0),tree_parent.rt_node) 
	print('X: ',x)
	if len(x)>0 and tree_add.the_job.taskno!=tree_parent.the_job.taskno :
		z=np.convolve(x,tree_add.the_job.dist)								#+ wcet_values_HI ( arc.to_job )  
#		print('Response Time Calculate: ',z)
		return z
	else:
		return tree_add.the_job.dist

def Calculate_RT_Dependent(tree_add,tree,tasknodep,jobnodep):
	s=0	
	rt=[]
	y=[]
	tree_check=Tree(-1,-1,tasks.Job_Discrete(-1,-1,-1,-1,-1),0)
	for i in range(4,len(tree_add.rt_node)):
		s=s+tree_add.rt_node[i]
		print('s: here: ',s)
	

	tree_check=tree[tree_add.from_node]
	while len(tree):
		print('In the loop !, RT here: ', tree_add.rt_node)
		if tree_check.the_job.taskno==tasknodep and tree_check.the_job.jobno==jobnodep:
			x=tail(4,tree_check.rt_node)
#			for i in range(1,len(xx)):
#				x.append(xx[i])
			yy=tail(4,tree_add.rt_node)
			for i in range(1,len(yy)):
				y.append(yy[i])
			z=np.convolve(x,y)
			print('Z: ',z)
#			z=z*s
#			print('Z after: ',z)
			for i in range(0,len(z)+4):
				if i<=4:
					rt.append(tree_add.rt_node[i])
				else:
					rt.append(z[i-4])
			print('GOT THE DEPENDENCE:',rt)
			return rt
			break
		else:
			print('Going back a step! ')
			tree_check=tree[tree_check.from_node]


		if tree_check.no<0:
			print('error')
			break
#		return tree_add.rt_node



def tail(time,rt):
		di=[]
		s=0
		print('at time: ',time, 'len: ',len(rt))
		if time < len(rt):
			print('returning to convolve')
			for i in range(0, time):   
				s=s+rt[i]
#			if time>0:
			print('s here /!!!!! :',s)
			if s>0:
				di.append(s)		
			for i in range(time, len(rt)):   #self.deadline-self.arrival):
				di.append(rt[i])		
#			for j in range(0,len(di)):
#				di[j]=di[j]/s
			print('di = ',di)
			return di 

		else:
			return []




def dm(element):
	s=0
	for i in range(element.the_job.deadline-element.the_job.arrival,len(element.rt_node)):
		s=s+element.rt_node[i]	
		print('VALUE OF s:',s)
	if s>1e-03:
		print('DEADLINE MISS!')
		return (s,1)
	else:
		return (s,0)

	

def threshold(element):
	s=0
	print('math.floor((element.the_job.deadline-element.the_job.arrival)*0.4: ',math.floor((element.the_job.deadline-element.the_job.arrival)*0.4))
	print('len(element.rt_node)) :',len(element.rt_node))

	if math.floor((element.the_job.deadline-element.the_job.arrival)*0.4)<len(element.rt_node):
		print('say yes !!!')
		for i in range(4,len(element.rt_node) ): #math.floor((element.the_job.deadline-element.the_job.arrival)*0.7),len(element.rt_node)):
			s=s+element.rt_node[i]	
	
	print('s:',s)
	return s if s>0 else 0









