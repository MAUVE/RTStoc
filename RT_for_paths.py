#!usr/bin/env python3
from matplotlib import pyplot as plt
import tasks
import operations as ops
import RT_Graphs
import os

os.system('rm Graph/*')


task=[]
#task.append(tasks.Task(1,10,2))		#task exp rate/WCET, crit 0=lo 1=hi

task.append(tasks.Task_Discrete(10,0))      #task period, crit 0=lo 1=hi
dis=[  0.1 , 0.3 , 0.5 , 0.094 , 0.005 , 0.001   ]					#distrubtion for each time=1,2,3,4,...
for i in range(0,len(dis)):						#represent with ZEROS if required
	task[0].dist.append(dis[i])

task.append(tasks.Task_Discrete(20,0))
dis= [0.1,0.4,0.4,0.1]				#[0.1,0.2,0.4,0.1,0.05,0.1,0.05]	
for i in range(0,len(dis)):
	task[1].dist.append(dis[i])

task.append(tasks.Task_Discrete(15,0))
dis=[0.1,0.4,0.3,0.2]	
for i in range(0,len(dis)):
	task[2].dist.append(dis[i])

task.append(tasks.Task_Discrete(30,0))
dis=[0.1,0.7,0.2]	
for i in range(0,len(dis)):
	task[3].dist.append(dis[i])








hp=ops.hyperperiod(task)
print('Hyperperiod= ',hp)

no_jobs=0
job=[]
for i in range (0,len(task)):
	no_jobs+=hp/task[i].period
print('Total number of jobs: ',no_jobs, 'where...')
for i in range (0,len(task)):
	print('Task ',i+1, 'has', hp/task[i].period ,'jobs')
	for j in range(0,int(hp/task[i].period)):
		job.append(tasks.Job_Discrete(i,j,task[i].dist,task[i].period,task[i].crit))






hi_jobs=[]
mi_jobs=[]
lo_jobs=[]
for i in range(0,len(job)):
	if job[i].crit==2:
		hi_jobs.append(job[i])	
	elif job[i].crit==1:
		mi_jobs.append(job[i])	
	else:
		lo_jobs.append(job[i])	
for i in range(0,len(hi_jobs)):
	hi_jobs[i].printparam()


no_nodes=0
for i in range(0,int(no_jobs)):
	for j in range(0,job[i].crit+1):
		no_nodes=no_nodes+1

print('\nJobs are (-1 indicates priority not assigned yet): ')	
tasks.printjobs_disc(job)


nodes=[]
k=0
for i in range(0,int(no_jobs)):

	print('i: ',i)
#	print('j: ',j)
	print('k: ',k)

	nodes.append(tasks.Job_Discrete(-1,-1,-1,-1,-1))
	nodes[k].taskno=job[i].taskno
	nodes[k].jobno=job[i].jobno
	nodes[k].dist=job[i].dist
	nodes[k].arrival=job[i].arrival
	nodes[k].deadline=job[i].deadline
	nodes[k].priority=job[i].priority



#		print('kbefore : ',k,'critbefore',job[i].crit)
	job[i].printparam()
	nodes[k].crit=job[i].crit

	print('nodes[',k,'].crit',nodes[k].crit)
	print('nodes[',k,'].taskno',nodes[k].taskno+1)
	print('nodes[',k,'].jobno',nodes[k].jobno+1)

	k=k+1


print('\n After')
for k in range(0,len(nodes)):
	print('nodes[',k,'].crit',nodes[k].crit)
	print('nodes[',k,'].arrival',nodes[k].arrival)
	print('nodes[',k,'].taskno',nodes[k].taskno+1)
	print('nodes[',k,'].jobno',nodes[k].jobno+1)

l1="digraph nodes { \n"
l3="\n}"
l2=" "
#for i  in range(0,len(nodes)):
#	l2=l2+ " \"  node_{}_{}_crit_{} \"  -> ".format(nodes[i].taskno+1,nodes[i].jobno+1,nodes[i].crit)

#print(l1+l2+l3)





print('nodes: ',len(nodes))



arc=[]
for i in range(0,len(nodes)):
	for j in range(0,len(nodes)):
#		if Graphs.issame(nodes[i],nodes[j])==0:
		if RT_Graphs.isok(nodes[i],nodes[j])==1 :
#		if i!=j:
			arc.append(RT_Graphs.Arc(nodes[i],nodes[j]))

#print(arc[1].nodes.jobno)




print('len: ',len(arc))


RT_Graphs.script_graph(arc,nodes)
	

r=len(task)


initial_node=[]

for i in range(0,len(nodes)):
	if nodes[i].arrival==0 :
		initial_node.append(nodes[i])
		print('i for initial',i)


for i in range(0, 1): #	len(initial_node)):
	print('----------------------------------------------------------------TREE NO ',i+1,'----------------------------------------------------------------')
	sen_br=('----------------------------------------------------------------TREE NO {}----------------------------------------------------------------').format(i+1)
	file = open("Graph/Valid and non dangerous paths",'a') 
	file.write(sen_br)
	file.close() 

	RT_Graphs.Construct_Model( initial_node[i],arc ,nodes,hi_jobs,mi_jobs,lo_jobs,hp,job,task)







