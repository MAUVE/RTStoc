import os
import itertools as it
import time 	
import re
import tasks

prism_path='~/Codes_Softwares/prism-4.4-linux64/bin/prism' 			#locate prism


#def MDP_script(job):	



def SSscript(job,SStaskno):
	no_jobs=0
	for i in range(0,len(job)):
		if job[i].taskno==SStaskno:				#< or ==
			no_jobs=no_jobs+1
	header='dtmc\n'
	states='x:[0..{}] init 0;\n'.format(no_jobs+1) 
	file1=open("RESULTS/Model_DTMC_{}".format(SStaskno),"a") 
	file1.write(header)
	file1.write("module M\n")
	file1.write(states)
	maxprob=tasks.Job(0,0,0,0)
	mxprob=0
	statecount=0
	for i in range(0,len(job)):
		if job[i].taskno==SStaskno:				#< or ==
			prop_dm='1-P=? [F={} x={}]\n'.format((job[i].deadline-job[i].arrival),0)
	#		else:
	#			prop_dm='1-P=? [F={} x={}]\n'.format((job.deadline-job.arrival),0)
		
			file = open("Prism_Files/Property_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"w") 
			file.write(prop_dm)
			file.close() 
			
			command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_Crit_{}_{} > Prism_Files/Result_Job_Crit_{}_{} ".format(prism_path,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno)				
			os.system(command)
			while not os.path.exists("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				time.sleep(1)
			if os.path.isfile("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno)):
				file = open("Prism_Files/Result_Job_Crit_{}_{}".format(job[i].taskno,job[i].jobno),"r")
				text=file.read()
				file.close()
				res_pos=text.find('Result') 	
				if res_pos==-1:
					prob=-1
				else:
					line=text[res_pos:res_pos+55]
					val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
					prob=float(val[0])	
			else:
			   	raise ValueError("%s isn't a file!" % file_path)
					

			if prob>mxprob and (job[i].taskno>0 or job[i].jobno>0) :
				mxprob=prob 
				maxprob=job[i]
#			comment="\\\\{},{}\n \n".format(job[i].taskno,job[i].jobno)
			print('prob: ',prob)
			if i==0:
				statement1="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,prob,1,1-prob,2) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,1,1,2) 
				statecount=statecount+2
			else:

	
				statement1="\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,prob,1,1-prob,statecount+1) if prob>0 else "\n[] x={} -> {}:(x'={}) + {}:(x'={}); \n".format(statecount,0,1,1,statecount+1) 
				statecount=statecount+1

			print('statecount', statecount)
			file1.write(statement1)
			if i==no_jobs:
				statement2="\n[] x={} -> 1:(x'={}) ;\n".format(statecount,0)
				statement3="\n[] x={} -> 1:(x'={}) ;\n".format(1,0)
				file1.write(statement2)
				file1.write(statement3)

	file1.write('endmodule')
	file1.close
	return maxprob,mxprob

def SS_analysis(SStaskno):

	prop='S=? [x=1] '
	file = open("Prism_Files/Property_MDP","w") 
	file.write(prop)
	file.close() 

	command="{} -cuddmaxmem 8g -gaussseidel  RESULTS/Model_DTMC_{} Prism_Files/Property_MDP  > RESULTS/Result_DTMC_{}".format(prism_path,SStaskno,SStaskno)
	os.system(command)
	while not os.path.exists("RESULTS/Result_DTMC_{}".format(SStaskno)):
		time.sleep(1)
	if os.path.isfile("RESULTS/Result_DTMC_{}".format(SStaskno)):
		file = open("RESULTS/Result_DTMC_{}".format(SStaskno),"r")
		text=file.read()
		file.close()
		res_pos=text.find('Result') 	
		if res_pos==-1:
			prob=-1
		else:
			line=text[res_pos:res_pos+55]
			val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
			prob=float(val[0])	
	else:
	   	raise ValueError("%s isn't a file!" % file_path)
			
	print('value: ', prob)

	print('Steady State Probability for Task ',SStaskno+1, ': ',prob)











