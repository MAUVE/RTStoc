#!usr/bin/env python3
from matplotlib import pyplot as plt
import tasks
import operations as ops
import Graphs
import os

os.system('rm Graph/*')


task=[]
#task.append(tasks.Task(2,10,1))		#task exp rate/WCET, crit 0=lo 1=hi
#task.append(tasks.Task(2,20,0))# --------------PUT WCET VALUS IN THE FUNCITON--------------
#task.append(tasks.Task(2,15,0))
#task.append(tasks.Task(2,30,0))
#task.append(tasks.Task(2,10,0))
#task.append(tasks.Task(2,30,0))

task.append(tasks.Task(2,10,1))
task.append(tasks.Task(2,20,1))
task.append(tasks.Task(2,20,1))
task.append(tasks.Task(2,20,1))
task.append(tasks.Task(2,20,1))
task.append(tasks.Task(2,20,1))
task.append(tasks.Task(2,20,1))

#task.append(tasks.Task(3,10,2))	
#task.append(tasks.Task(3,30,1))
#task.append(tasks.Task(3,15,0))
#task.append(tasks.Task(3,30,0))



hp=ops.hyperperiod(task)
print('Hyperperiod= ',hp)

no_jobs=0
job=[]
for i in range (0,len(task)):
	no_jobs+=hp/task[i].period
print('Total number of jobs: ',no_jobs, 'where...')
for i in range (0,len(task)):
	print('Task ',i+1, 'has', hp/task[i].period ,'jobs')
	for j in range(0,int(hp/task[i].period)):
		job.append(tasks.Job(i,j,task[i].prate,task[i].period,task[i].crit))





#job[1].crit=1
#job[2].crit=2
#job[3].crit=0
#job[4].crit=0

hi_jobs=[]
mi_jobs=[]
lo_jobs=[]
for i in range(0,len(job)):
	if job[i].crit==2:
		hi_jobs.append(job[i])	
	elif job[i].crit==1:
		mi_jobs.append(job[i])	
	else:
		lo_jobs.append(job[i])	
for i in range(0,len(hi_jobs)):
	hi_jobs[i].printparam()


no_nodes=0
for i in range(0,int(no_jobs)):
	for j in range(0,job[i].crit+1):
		no_nodes=no_nodes+1

print('\nJobs are (-1 indicates priority not assigned yet): ')	
tasks.printjobs(job)


nodes=[]
k=0
for i in range(0,int(no_jobs)):
	for j in range(0,job[i].crit+1):
		print('i: ',i)
		print('j: ',j)
		print('k: ',k)

		nodes.append(tasks.Job(-1,-1,-1,-1,-1))
		nodes[k].taskno=job[i].taskno
		nodes[k].jobno=job[i].jobno
		nodes[k].rate=job[i].rate
		nodes[k].arrival=job[i].arrival
		nodes[k].deadline=job[i].deadline
		nodes[k].priority=job[i].priority



#		print('kbefore : ',k,'critbefore',job[i].crit)
		job[i].printparam()

		nodes[k].crit=j

		print('nodes[',k,'].crit',nodes[k].crit)
		print('nodes[',k,'].taskno',nodes[k].taskno+1)
		print('nodes[',k,'].jobno',nodes[k].jobno+1)

		k=k+1


print('After')
for k in range(0,len(nodes)):
	print('nodes[',k,'].crit',nodes[k].crit)
	print('nodes[',k,'].arrival',nodes[k].arrival)
	print('nodes[',k,'].taskno',nodes[k].taskno+1)
	print('nodes[',k,'].jobno',nodes[k].jobno+1)

l1="digraph nodes { \n"
l3="\n}"
l2=" "
#for i  in range(0,len(nodes)):
#	l2=l2+ " \"  node_{}_{}_crit_{} \"  -> ".format(nodes[i].taskno+1,nodes[i].jobno+1,nodes[i].crit)

#print(l1+l2+l3)





print('nodes: ',len(nodes))



arc=[]
for i in range(0,len(nodes)):
	for j in range(0,len(nodes)):
#		if Graphs.issame(nodes[i],nodes[j])==0:
		if Graphs.isok(nodes[i],nodes[j])==1 :
#		if i!=j:
			arc.append(Graphs.Arc(nodes[i],nodes[j]))

#print(arc[1].nodes.jobno)




print('len: ',len(arc))


Graphs.script_graph(arc,nodes)
	

r=len(task)


initial_node=[]

for i in range(0,len(nodes)):
	if nodes[i].arrival==0 and  nodes[i].crit==0:
		initial_node.append(nodes[i])
		print('i for initial',i)


for i in range(0, 1): #	len(initial_node)):
	print('----------------------------------------------------------------TREE NO ',i+1,'----------------------------------------------------------------')
	sen_br=('----------------------------------------------------------------TREE NO {}----------------------------------------------------------------').format(i+1)
	file = open("Graph/Valid and non dangerous paths",'a') 
	file.write(sen_br)
	file.close() 

	Graphs.Construct_Model( initial_node[i],arc ,nodes,hi_jobs,mi_jobs,lo_jobs,hp,job,task)







