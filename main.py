#!usr/bin/env python3
import tasks
import operations as ops
import prism_files as prism
import os
import MDP
import MDP_Dep
import rt_curve
import time
import SteadyState
start = time.time()

#os.system('rm Result')
os.system('rm Prism_Files/*')
os.system('rm RESULTS/*')


#SKIP=0			#if =1, it skips the model construction, straight to model checkin, USE WITH CAUTION


#----------------task set--------------
task=[]
task.append(tasks.Task(12,3))		#task exp rate, deadline=period / IN THE ORDER IF FP
task.append(tasks.Task(13,5))
task.append(tasks.Task(15,6))
task.append(tasks.Task(18,10))
#task.append(tasks.Task(15,10))
<<<<<<< HEAD
#task.append(tasks.Task(16,15))
hp=ops.hyperperiod(task)
print('Hyperperiod= ',hp)

=======
>>>>>>> c3e874bc7dc141cfe4b8e921c31ee24fd9894c96
					# 16 is the limit for PRISM
no_crit=1				#no_crit=no_crit-1
threshold= 0.7 #1			#as a factor of deadline
SStaskno=0

policy='EDF' 				#EDF/FP
abort=True				#Must be kept True
preemption=True				#Must be kep True
FoS=1					#FoS
#k=9					#For mixed criticality

tasks.printtasks(task)
hp=ops.hyperperiod(task)
print('Hyperperiod= ',hp)

#----------------creation of jobs--------------
no_jobs=0
job=[]
for i in range (0,len(task)):
	no_jobs+=hp/task[i].period
print('Total number of jobs: ',no_jobs, 'where...')
for i in range (0,len(task)):
	print('Task ',i+1, 'has', hp/task[i].period ,'jobs')
	for j in range(0,int(hp/task[i].period)):
		job.append(tasks.Job(i,j,task[i].prate,task[i].period,[0.0]*int(no_jobs)))
print('\nJobs are (-1 indicates priority not assigned yet): ')	
tasks.printjobs(job)
print('\nAfter',policy,'Scheduling in order of priority: ')


job=ops.prioritize_jobs(job,policy,no_jobs)

tasks.printjobs(job)

#----------------specific job depdence tuples--------------


#for i in range(0,int(no_jobs)-1):
#	if i==int(no_jobs)-2:
#		job[i].dep[i+1]=0.2
#
#	elif i==no_jobs-3:
#		job[i].dep[i+1]=0.2
#		job[i].dep[i+2]=0.1
#
#	else:
#		job[i].dep[i+1]=0.2
#		job[i].dep[i+2]=0.1
#		job[i].dep[i+3]=0.02
#	print('Dependence: ',job[i].dep)#, 'i: ' , i+1)


#print('Dependence: ',job[int(no_jobs)-1].dep)

Jtilde=[]
Jtilde=[tasks.Job(0,0,0,0,[0.0]*int(no_jobs))]
Jbar=[]
Jhat=[]

 
print('\n-----------------Constructing Models-----------------\n')

#----First job----
job[0].printparam()
prism.prism_script(job[0],[],[],[],abort,preemption,FoS)

#-----Second job onwards------
#job[21].rate=0

#job[5].rate=0
#job[6].rate=0
#job[8].rate=0

#job[23].rate=0
#job[24].rate=0
#job[22].rate=0
#job[26].rate=0

#job[10].rate=0
#job[11].rate=0

#job[1].rate=0

#job[13].rate=0
#job[15].rate=0
#job[16].rate=0

for i in range(1,len(job)):
	job[i].printparam()
	Jbar[:]=[]
	Jhat[:]=[]
	Jtilde[:]=[]
	pdiff=no_jobs
	for j in range(0,len(job)):
		p=job[i].priority-job[j].priority if job[i].priority-job[j].priority >0 else 0
#		print('p: ',p)
		if (job[j].priority<job[i].priority and job[j].arrival<job[i].arrival and job[j].deadline>job[i].arrival and job[j].rate!=0 and p<pdiff): 
			Jtilde=[job[j]]
			print('Jtilde: Task ',job[j].taskno+1, ' job: ',job[j].jobno+1)
			
			pdiff=p

		elif (job[j].priority<job[i].priority and job[j].arrival>job[i].arrival and job[j].arrival<job[i].deadline and job[j].rate!=0):
			flag=0
			for k in range(0,len(Jhat)):
				if Jhat[k].arrival==job[j].arrival:
					Jhat[k]=job[j]				# put the new one		
					flag=1
			if flag==0 and job[j].rate!=0:
				Jhat.append(job[j])					

		elif (job[j].priority<job[i].priority and job[j].arrival==job[i].arrival and job[j].rate!=0): 

			Jbar.append(job[j])
			print('Jbar: Task ',job[j].taskno+1, ' job: ',job[j].jobno+1)
	for j in range(0,len(Jhat)):
		for k in range(j,len(Jhat)):
			if Jhat[j].priority > Jhat[k].priority:
				jobtmp=Jhat[j]
				Jhat[j]=Jhat[k]
				Jhat[k]=jobtmp
				



	prism.prism_script(job[i],Jtilde,Jbar,Jhat,abort,preemption,FoS)		

print('Models Complete\n')
print('-----------------Analyzing-----------------\n')

print('\n-----Finding the probability of deadline miss for each job\n')
for i in range(len(job)):
	prism.prob_DM(job[i])
print('\nProbability of deadline miss written in file: RESULTS/Prob_DM')


print('\n-----Finding the Response Time Curve for each job\n')
rt_curve.rtcalc(job)
print('Response Time CDFs written in file: RESULS/RT_Job_<tasknumber>_<jobnumber>\n')

#input('Press Enter to exit')



#------------------------------------MIXED CRITICALITY------------------------------------

#MDP.prob_back(job)


#print('\n-----Finding the probability of entering high criticality for each job\n')

	
#maxprob,mxprob=MDP.Criticality_script(job,threshold,no_crit)
#MDP.Criticality_analysis(job,no_crit)

#print('\nProbability of deadline miss written in file: RESULTS/Prob_Criticality')
#print('\nMax prob of task and job: ' )
#maxprob.printparam()


#dropjob=tasks.Job(0,0,0,0)
#for i in range (maxprob.priority,0):
#	if job[i].priority<maxprob.priority and job[i].taskno>=no_crit:
#		dropjob=job[i]
#		break


#print('Drop: ')
#dropjob.printparam()

#SteadyState.SSscript(job,SStaskno)
#SteadyState.SS_analysis(SStaskno)

#------------------------------------MIXED CRITICALITY DEPENDENCE------------------------------------
print('HERE')

maxprob,mxprob=MDP_Dep.Criticality_script(job,threshold,no_crit,no_jobs)
MDP_Dep.Criticality_analysis(job,no_crit)


end = time.time()
print('Analysis took : ', end - start, 'seconds')
#print('For deadline miss of task 1')






