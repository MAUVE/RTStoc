#!usr/bin/env python3
import tasks
import operations as ops
import analyse

#----------------task set--------------
task=[]
task.append(tasks.Task(7,1))		#task exp rate, deadline=period / IN THE ORDER IF FP

no_iter=100
threshold=0.7

policy='EDF' 				#EDF/FP
abort=True				#Must be kept True
preemption=True				#Must be kep True

tasks.printtasks(task)
hp=ops.hyperperiod(task)
print('Hyperperiod= ',hp)

#----------------creation of jobs--------------
no_jobs=2
job=[]
for i in range (0,len(task)):
	no_jobs+=hp/task[i].period
print('Total number of jobs: ',no_jobs, 'where...')
for i in range (0,len(task)):
	print('Task ',i+1, 'has', hp/task[i].period ,'jobs')
	for j in range(0,int(hp/task[i].period)):
		job.append(tasks.Job(i,j,task[i].prate,task[i].period))
print('\nJobs are (-1 indicates priority not assigned yet): ')	
tasks.printjobs(job)
print('\nAfter',policy,'Scheduling in order of priority: ')


job=ops.prioritize_jobs(job,policy)

tasks.printjobs(job)


#----------------Analysis--------------
analyse.iterate(job,no_iter,no_jobs)


