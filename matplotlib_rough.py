from matplotlib import pyplot as plt


x=[1,2,3,4,5,6,7,8,9]
y=[4,8,5,1,3,6,2,4,7]
z=[2,4,5,7,5,9,6,1,7]
fig = plt.figure()
ax = fig.add_subplot(111)
lines=ax.plot(x,y,marker='', color='blue', linewidth=1, alpha=1) 
lines.append(ax.plot(x,z,marker='', color='red', linewidth=4, alpha=1) )
print(lines)
xpoint_sel=0
ypoint_sel=0

def onclick(event):
	print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          ('double' if event.dblclick else 'single', event.button,
           event.x, event.y, event.xdata, event.ydata))
	xpoint_sel=event.xdata
	ypoint_sel=event.ydata

	for i in range(0,len(x)):
		if x[i]-0.5<xpoint_sel<x[i]+0.5:
			for i in range(0,len(x)):
				if y[i]-0.5<ypoint_sel<y[i]+0.5:
					print('Near to', x[i],y[i])
#	line = ax.plot(x,y,marker='', color='blue', linewidth=4, alpha=1) 
#	line = ax.plot(x,z,marker='', color='red', linewidth=1, alpha=1) 
#	ax.remove(line2)
	#ax.lines.pop(0)
	
	lines[0]=ax.plot(x,y,marker='', color='red', linewidth=4, alpha=1) 
	lines[1]=ax.plot(x,z,marker='', color='blue', linewidth=1, alpha=1) 

	plt.draw()


	
		
cid = fig.canvas.mpl_connect('button_press_event', onclick)

plt.show()

