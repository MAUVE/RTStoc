import numpy as np
import tasks
from fractions import gcd

def hyperperiod(task):
	hp =task[0].period
	for i in range(0,len(task)):
		hp = hp*task[i].period/gcd(hp, task[i].period)
	return hp 


def prioritize_jobs(job,policy):
	if policy=='EDF':
		tmp=tasks.Job(0,0,0,0)
		k=0
		for i in range(0,len(job)):
			for j in range(i+1,len(job)):
				if job[j].deadline<job[i].deadline:
					tmp=job[i]
					job[i]=job[j]
					job[j]=tmp
				elif job[j].deadline==job[i].deadline:
					if job[j].arrival<job[i].arrival:
						tmp=job[i]
						job[i]=job[j]
						job[j]=tmp
			job[i].priority=k
			k+=1
	elif policy=='FP':
		p=0;
		for j in range(0,len(job)):
			job[j].priority=p
			p=p+1
			
			

	else:
		print("I don't know this policy yet, sorry. \n Let the developers know at: .....@...")
	return(job)	
	


