import numpy as np
import os
import time
import re
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 


prism_path='~/prism-4.4.beta-linux64/bin/prism' 			#locate prism


# TODO: Check prism install with (os.system('prism') == 0)


def prism_script(job,Jtilde,Jbar,Jhat,abort,preemption,FoS):
	no_pre=0 if (Jhat==[0] or preemption==False) else len(Jhat)
	Lambda=backlog(job,Jtilde,Jbar,FoS) 
	Q=np.array([[0.0,Lambda],[0.0,0.0]])
	header='ctmc\n'
	states='x:[0..{}] init 1;\n'.format(no_pre+1) 
	for i in range(0,no_pre+1):
		file = open("Prism_Files/Model_Job_{}_{}".format(job.taskno,job.jobno),"w") 
		file.write(header)
		file.write("module M\n")
		file.write(states)
		if i==0:
			statement="\t[] x={} -> {}:(x'={}); ".format(i+1,Q[i,i+1],0)	
			file.write(statement) 
		else:		
			statement=''
			for j in range(0,i):
				statement+="\t[] x={} -> {}:(x'={}) + {}:(x'={}) ; \n ".format(j+1,Q[j,j+1],j+2,Q[j,-1],0)
			statement+="\t[] x={} -> {}:(x'={});\n ".format(i+1,Q[-2,-1],0)	
			file.write(statement) 
		file.write("\nendmodule\n")
		file.close() 
		
		#----Property file----
		if Jhat==[0]:
			timechk=job.deadline-job.arrival
		elif Jhat !=[0] and i==no_pre:
			timechk=job.deadline-job.arrival
		else:
			timechk=Jhat[i].arrival-job.arrival
		prop='P=? [F={} x={}]\n'.format(timechk,i+1)
		file = open("Prism_Files/Property_Job_{}_{}".format(job.taskno,job.jobno),"w") 
		file.write(prop)
		file.close() 

		if no_pre>0  and i<no_pre:
			prism_check(job)
			Pr=read_result(job)
			lam_pnew=Lambda*Pr
			lam_fnew=Lambda-lam_pnew
			Q=np.pad(Q,((0,1),(0,1)),'constant',constant_values=0)	
			Q[i,i+1]=lam_pnew
			Q[i,i+2]=lam_fnew
			if i>0:
				k=i
				for j in range(1,i+1):
					Q[j-1,j+k+1]=Q[j-1,j+k]
					Q[j-1,j+k]=0
					k=k-1


			Q[i+1,i+2]=Delta(job,Jhat,i+1,abort)
		

def Delta(job,Jhat,prempno,abort):					
	if abort:
		endcomment= Jhat[prempno-1].deadline-job.arrival
	else:
		endcomment=1000						#If ABORT is false

	begcomment=Jhat[prempno-1].arrival-job.arrival

	prop_delta='P=? [F[{},{}] x={}]\n'.format(begcomment,endcomment,0)

	file = open("Prism_Files/Property_Job_Deltaextenstion_{}_{}".format(job.taskno,job.jobno),"w") 
	file.write(prop_delta)
	file.close() 


	command_delta="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_Deltaextenstion_{}_{} > Prism_Files/Result_Job_Deltaextension_{}_{} ".format(prism_path,job.taskno,job.jobno,job.taskno,job.jobno,job.taskno,job.jobno)				
	os.system(command_delta)

	
	while not os.path.exists("Prism_Files/Result_Job_Deltaextension_{}_{}".format(job.taskno,job.jobno)):
		time.sleep(1)
	if os.path.isfile("Prism_Files/Result_Job_Deltaextension_{}_{}".format(job.taskno,job.jobno)):
		file = open("Prism_Files/Result_Job_Deltaextension_{}_{}".format(job.taskno,job.jobno),"r")
		text=file.read()
		file.close()
		res_pos=text.find('Result') 
		if res_pos==-1:
			return -1
		else:
			line=text[res_pos:res_pos+55]
			value=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))
			return np.log(     (1-float(value[0]) * (1-np.exp(-Jhat[prempno-1].rate*(Jhat[prempno-1].deadline-Jhat[prempno-1].arrival)) ))     )*(-1/(Jhat[prempno-1].deadline-Jhat[prempno-1].arrival))

	else:
	   	raise ValueError("%s isn't a file!" % file_path)





def backlog(job,Jtilde,Jbar,FoS):

	d=10000			

	FoS=1/2
	if len(Jtilde)==0 and len(Jbar)>0:
		lim=(job.deadline-job.arrival)*FoS
		x=np.linspace(0,lim,d*lim)
	
		exp1=Jbar[0].rate*np.exp(-x*Jbar[0].rate)
		for i in range(1,len(Jbar)):
			exp2=Jbar[i].rate*np.exp(-Jbar[i].rate*x)
#			if exp2.all() !=0 and exp1.all() !=0:
			exp1=np.convolve(exp2,exp1)
		exp=job.rate*np.exp(-x*job.rate)
#		if exp.all() !=0 and exp1.all() !=0:
		res=np.convolve(exp,exp1)
		res=res[0:int(lim*d)]		
		integration=np.trapz(res,dx=1/(d**(len(Jbar)+1)))
		if integration <1:
			return (-1/lim) * np.log(1-integration) 
		else:
			acc=(((0.099-(1-integration))/0.099)*999)*0.00000000000001
			return (-1/lim)*np.log(1-(0.99999999+acc))


	elif len(Jbar)==0 and len(Jtilde)>0:
		t=job.deadline-job.arrival
		x=np.linspace(0,t,d*t)
		exp=job.rate*np.exp(-job.rate*x)
		return (-1/(job.deadline-job.arrival)) * np.log(1- (((delta_back(job,Jtilde))) *(np.trapz(exp,dx=1/(d**(len(Jbar)+1))))))

	elif len(Jbar)==0 and len(Jtilde)==0:
		return job.rate
	else:
		t=job.deadline-job.arrival
		x=np.linspace(0,t,d*t)
		exp1=Jbar[0].rate * np.exp(-Jbar[0].rate * x)
		for i in range(1,len(Jbar)):
			exp2=Jbar[i].rate*np.exp(-Jbar[i].rate*x)
#			if exp2.all() !=0 and exp1.all() !=0:
			exp1=np.convolve(exp2,exp1)
		exp=job.rate * np.exp(-job.rate * x)
#		if exp2.all() !=0 and exp1.all() !=0:
		res=np.convolve(exp1,exp)
		integration=np.trapz(res,dx=1/(d**(len(Jbar)+1)))
		if integration<1:
			return (-1/(job.deadline-job.arrival)) * np.log(1-((delta_back(job,Jtilde))*integration))
		else:
			acc=((0.099-(1-integration))/0.099)*999*0.00000000000001
			return (-1/(job.deadline-job.arrival)) * np.log(1-((delta_back(job,Jtilde)) * (0.99999999+acc))) 


def delta_back(job,Jtilde):	
	prop_back='P=? [F[{},{}] x={}]'.format(job.arrival-Jtilde[0].arrival, Jtilde[0].deadline-Jtilde[0].arrival, 0)
	file = open("Prism_Files/Property_Job_Delta_back_{}_{}".format(job.taskno,job.jobno),"w") 
	file.write(prop_back)
	file.close() 
	command_back="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_Delta_back_{}_{} > Prism_Files/Result_Job_Delta_back_{}_{} ".format(prism_path,Jtilde[0].taskno,Jtilde[0].jobno,job.taskno,job.jobno,job.taskno,job.jobno)				
	os.system(command_back)
	
	while not os.path.exists("Prism_Files/Result_Job_Delta_back_{}_{}".format(job.taskno,job.jobno)):
		time.sleep(1)
	if os.path.isfile("Prism_Files/Result_Job_Delta_back_{}_{}".format(job.taskno,job.jobno)):
		file = open("Prism_Files/Result_Job_Delta_back_{}_{}".format(job.taskno,job.jobno),"r")
		text=file.read()
		file.close()
		res_pos=text.find('Result') 

		if res_pos==-1:
			return -1
		else:
			line=text[res_pos:res_pos+55]
			val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))
			return float(val[0])
	else:
	   	raise ValueError("%s isn't a file!" % file_path)




def prism_check(job):
	command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_{}_{} > Prism_Files/Result_Job_{}_{} ".format(prism_path,job.taskno,job.jobno,job.taskno,job.jobno,job.taskno,job.jobno)				
	os.system(command)
	

def read_result(job):
	while not os.path.exists("Prism_Files/Result_Job_{}_{}".format(job.taskno,job.jobno)):
		time.sleep(1)
	if os.path.isfile("Prism_Files/Result_Job_{}_{}".format(job.taskno,job.jobno)):
		file = open("Prism_Files/Result_Job_{}_{}".format(job.taskno,job.jobno),"r")
		text=file.read()
		file.close()
		res_pos=text.find('Result') 
		if res_pos==-1:
			return -1
		else:
			line=text[res_pos:res_pos+55]
			val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+",line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))
			return float(val[0])

	else:
	   	raise ValueError("%s isn't a file!" % file_path)
	



def prob_DM(job):
	prop_dm='1-P=? [F={} x={}]\n'.format((job.deadline-job.arrival),0)
	file = open("Prism_Files/Property_Job_DM_{}_{}".format(job.taskno,job.jobno),"w") 
	file.write(prop_dm)
	file.close() 
	
	command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_DM_{}_{} > Prism_Files/Result_Job_DM_{}_{} ".format(prism_path,job.taskno,job.jobno,job.taskno,job.jobno,job.taskno,job.jobno)				
	os.system(command)
	while not os.path.exists("Prism_Files/Result_Job_DM_{}_{}".format(job.taskno,job.jobno)):
		time.sleep(1)
	if os.path.isfile("Prism_Files/Result_Job_DM_{}_{}".format(job.taskno,job.jobno)):
		file = open("Prism_Files/Result_Job_DM_{}_{}".format(job.taskno,job.jobno),"r")
		text=file.read()
		file.close()
		res_pos=text.find('Result') 

		if res_pos==-1:
			prob=-1
		else:
			line=text[res_pos:res_pos+55]
			val=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\.+\d+\E-\d+", line))	
			prob=float(val[0])

	else:
	   	raise ValueError("%s isn't a file!" % file_path)
			

	print('Task: {} Job: {} = {}'.format(job.taskno+1,job.jobno+1,prob))
	prob_res='{},{},{} \n'.format(job.taskno,job.jobno,prob)
	file = open("RESULTS/Prob_DM".format(job.taskno,job.jobno),"a") #Job_taskno_jobno
	file.write(prob_res)
	file.close() 





