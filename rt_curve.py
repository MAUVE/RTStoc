import numpy as np
import os
import time
import matplotlib.pyplot as plt
import re

prism_path='~/Codes_Softwares/prism-4.4-linux64/bin/prism' 			#locate prism

def rtcalc(job):
	for i in range(0,len(job)):
		t=np.linspace(0,job[i].deadline-job[i].arrival,20*(job[i].deadline-job[i].arrival))
		pnt=[]
		print('Task:',job[i].taskno,'Job:',job[i].jobno)
		for j in range(0,len(t)):
			file = open("Prism_Files/Property_Job_RT_{}_{}".format(job[i].taskno,job[i].jobno),"w") #Job_taskno_jobno
			prop_rt='P=? [F<={} x={}]\n'.format(t[j],0)
			file.write(prop_rt)
			file.close() 
	
			command="{} Prism_Files/Model_Job_{}_{} Prism_Files/Property_Job_RT_{}_{} > Prism_Files/Result_Job_RT_instant_{}_{} ".format(prism_path,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno,job[i].taskno,job[i].jobno)				#prism path should be configured
			os.system(command)


			while not os.path.exists("Prism_Files/Result_Job_RT_instant_{}_{}".format(job[i].taskno,job[i].jobno)):
				time.sleep(1)
			if os.path.isfile("Prism_Files/Result_Job_RT_instant_{}_{}".format(job[i].taskno,job[i].jobno)):
				file = open("Prism_Files/Result_Job_RT_instant_{}_{}".format(job[i].taskno,job[i].jobno),"r")
				text=file.read()
				file.close()
				res_pos=text.find('Result') 
				if res_pos==-1:
					value=0
				else:
					line=text[res_pos:res_pos+55]
					value=(re.findall("\d+\.\d+", line)) if len(re.findall("\d+\.+\d+\E-\d+", line))==0 else (re.findall("\d+\E-\d+", line))
					print('time',t[j])

			else:
			   	raise ValueError("%s isn't a file!" % file_path)

			pnt.append(value)
			file = open("RESULTS/RT_Job_{}_{}".format(job[i].taskno,job[i].jobno),"a") #Job_taskno_jobno
			value_file='{},{}\n'.format(t[j],float(value[0]))
			file.write(value_file)
			file.close() 

#		plt.ioff()
#		plt.figure(i)
#		lbl="Job_{}_{}".format(job[i].taskno,job[i].jobno)
#		plt.plot(t,pnt)
#		plt.title(lbl)
#		plt.ylabel('CDF')
#		plt.xlabel('RT random varible')
#		plt.show(block=False)

	
#def rtplot(job):
#	for i in range(0,len(job)):
#		t=np.linspace(0,job[i].deadline-job[i].arrival,10)
#	
#		file = open("RESULT_RT_Job_{}_{}".format(job[i].taskno,job[i].jobno),"r") #Job_taskno_jobno
#		text_rt=file.read()
#		file.close() 





