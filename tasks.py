class Task:
	def __init__(self,prate,period,crit):
		self.prate=prate
		self.period=period
		self.crit=crit



class Job:
	def __init__(self,taskno,jobno,rate,period,crit):
		self.taskno=taskno
		self.jobno=jobno
		self.rate=rate
		self.arrival=jobno*period
		self.deadline=period+(jobno*period)
		self.priority=-1
#		self.dep=dep
		self.crit=crit

	def printparam(self):
		print('\nTask:',self.taskno+1,'Job:',self.jobno+1,'lambda=',self.rate,'arrival time=',self.arrival,'deadline=',self.deadline,'priority=',self.priority,'crit=',self.crit)

def printtasks(task):
	for i in range (0,len(task)):
		print('Task ',i+1,': lambda=',task[i].prate,'; period=',task[i].period,'; crit=',task[i].crit)

def printjobs(job):
	for i in range (0,len(job)):
		print('Task:',job[i].taskno+1,'Job:',job[i].jobno+1,'lambda=',job[i].rate,'arrival time=',job[i].arrival,'deadline=',job[i].deadline,'priority=',job[i].priority,'crit=',job[i].crit)

def printjobs_disc(job):
	for i in range (0,len(job)):
		print('Task:',job[i].taskno+1,'Job:',job[i].jobno+1,'dist=',job[i].dist,'arrival time=',job[i].arrival,'deadline=',job[i].deadline,'priority=',job[i].priority,'crit=',job[i].crit)



class Task_Discrete:
	def __init__(self,period,crit):
		self.dist=[]
		self.period=period
		self.crit=crit


class Job_Discrete:
	def __init__(self,taskno,jobno,dist,period,crit):
		self.taskno=taskno
		self.jobno=jobno
		self.dist=dist
		self.arrival=jobno*period
		self.deadline=period+(jobno*period)
		self.priority=-1
#		self.dep=dep
		self.crit=crit

	def printparam(self):
			print('\nTask:',self.taskno+1,'Job:',self.jobno+1,'dist=',self.dist,'arrival time=',self.arrival,'deadline=',self.deadline,'priority=',self.priority,'crit=',self.crit)




